S1<br>

I _agree/disagree that_ based on a couple of reasons.
To begin with, I believe that _Reason 1_. For example, _EX1_. 

On top of that, I think that _Reason 2_. For instance, _EX2_.

(This is why I feel this way.)

S2<br>

The campus announcement mentions that/ The letter (that) the student wrote
mentions that _R _.

The man/woman in the listening agrees/,however, disagrees with it and
claims that _. 

First, he/she believes that _ 1_.

Second, he/she thinks that _ 2_.

(This is why he or she feels ...)

S3<br>

The reading passage discusses the concept of _Idea/Concept_.
It affirms _Definition_.
The professor in the lecture gives a perfect example.
_Listening Example_.

(The example clearly illustrates the idea of _.)

S4<br>

The lecturer discusses _Topic_._Supporting Details_. 
First, the professor states that _Point A_. 
Second, the professor notes that _Point B_. 
(These are the 2 _points_ provided in the lecture.) 
